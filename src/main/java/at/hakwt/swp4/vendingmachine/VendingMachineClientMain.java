package at.hakwt.swp4.vendingmachine;

import at.hakwt.swp4.vendingmachine.revenue.RevenueDao;
import at.hakwt.swp4.vendingmachine.storage.AustrianDrinkStorage;
import at.hakwt.swp4.vendingmachine.storage.DrinkStorageDao;
import at.hakwt.swp4.vendingmachine.model.Order;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class VendingMachineClientMain {

    public static void main(String[] args) {

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("ApplicationContext.xml");
        VendingMachineService service = applicationContext.getBean("vendingMachineService", VendingMachineService.class);

        Order order = applicationContext.getBean("order01", Order.class);
        System.out.println(order);
        Order order2 = applicationContext.getBean("order01", Order.class);
        System.out.println(order2);
        Order order3 = new Order("Cola", 2);
        System.out.println(order3);
        //VendingMachineService service = new DefaultVendingMachineService();

        for (int i = 0; i < 10; i++) {
            service.drinkSold("Cola 0.3", 1.99d);
            service.drinkSold("Römerquelle 0.5", 0.99d);
        }
        System.out.println("Heutiger Umsatz: " + service.getDailyRevenue());


    }

}
