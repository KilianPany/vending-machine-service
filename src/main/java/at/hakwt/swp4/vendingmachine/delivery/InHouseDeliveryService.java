package at.hakwt.swp4.vendingmachine.delivery;

import at.hakwt.swp4.vendingmachine.model.Drink;

import java.util.List;

public class InHouseDeliveryService implements DeliveryService {
    @Override
    public void deliver(List<Drink> drinks) {

        for (int i = 0; i < drinks.size(); i++) {

            System.out.println("Delivering " + drinks.size() + " drinks: " + drinks.get(i).getName());
        }
    }
}