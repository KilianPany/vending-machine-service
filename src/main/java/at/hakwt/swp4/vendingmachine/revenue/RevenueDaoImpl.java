package at.hakwt.swp4.vendingmachine.revenue;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class RevenueDaoImpl implements RevenueDao {

    private Map<LocalDate, Double> database;

    public RevenueDaoImpl() {

        this.database = new HashMap<>();
    }

    @Override
    public void registerRevenue(double price, LocalDate date) {
        if ( this.database.containsKey(date) ) {
            Double sum = this.database.get(date);
            this.database.put(date, sum+price);
        } else {
            this.database.put(date, price);
        }
    }

    @Override
    public Double getDailyRevenue(LocalDate date) {
        return database.get(date);
    }
}